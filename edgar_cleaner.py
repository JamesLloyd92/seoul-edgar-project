# This function can effectively clean direct .html links from the web,
# or it can clean downloaded html files also.

# First block avoids anonymous spam filters and scrapes the target webtext.

def clean_html_text(url=str):
    import edgar_cleaner as ec
    import requests
    from bs4 import BeautifulSoup
    import re
    import bs4
    import lxml.html

    if url.startswith('http'):
        user_agent = r'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36'
        url= rf'{url}'
        r = requests.get(url, headers={'User-Agent': user_agent})
        soup = BeautifulSoup(r.text,'html.parser')
        soup_text = soup.get_text()
        results = []

    # This below then splits the results and adds it to a list.
    # Then it cleans the text with a html parser and a reg expression.

        for element in soup_text.split():
            results.append(element)
        for k in str(results).splitlines():
            nospecial = re.sub(r"[^<>a-zA-Z0-9]+", ' ', k)
        parserObj = bs4.BeautifulSoup(nospecial, features="lxml")
        outputString = parserObj.get_text()
        return outputString
    
    # If cleaning a local file, the function doesn't need to get webdata so just cleans.
    # This function uses an extra regular expression to clean since the html parser 
    # doesn't work as well on local files.
    else:
        results = []
        for element in url.split():
            results.append(element)
        for k in str(results).splitlines():
            nospecial = re.sub(r"[^<>a-zA-Z0-9]+", ' ', k)
        parserObj = bs4.BeautifulSoup(nospecial, features="lxml")
        outputString = parserObj.get_text()
        c = re.sub(r'<.+?>', '', outputString)
        return c




# The below function will take files from an input folder, clean the text in them
# and write text files to the destination folder with the cleaned data.

def write_clean_html_text_files(input_folder, dest_folder):
    import edgar_cleaner as ec
    import os

    # Below gets the file names in the input directory

    directory = os.fsencode(input_folder)
    raw_list = []    
    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        raw_list.append(filename)

    # This first element in the loop below opens all thd files in the input folder for reading.

    for rawfile in raw_list:
        input_path = input_folder + rawfile 
        current_file = open(input_path, 'r', errors="ignore")
        readable = current_file.readlines()

    # This element in the loop does some extra cleaning and adds the text of each file into a 
    # new list. That list is then cast as a string and cleaned with the cleaning function above.

        wordlist = []
        for word in readable:
                wordlist.append(word.replace('\n', ''))
        wordlist = str(wordlist)
        current_file.close()
        results = ec.clean_html_text(wordlist)

    # This element of the loop writes the cleaned text into new .txt files in the destination folder.

        filename = f'{rawfile}.txt'
        x = os.path.join(dest_folder, filename)
        with open(x, "w") as file:
            file.writelines(results)
            file.close
        current_file.close()



# e.g., running the code below cleans the file specified as url:
# import edgar_cleaner as ec
# ec.write_clean_html_text_files(url)

# Below, the write clean text function will clean and produce text from .txts and .htmls in a target folder.
# import edgar_cleaner as ec
# write_clean_html_text_files('../seoul_edgar_project/10k_filings_raw/', '../seoul_edgar_project/10k_filings_clean/')