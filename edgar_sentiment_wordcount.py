import glob
import os
import re
from datetime import datetime
import ref_data as rd
import pandas as pd

import os

def write_document_sentiments(input_folder, output_file):
    #input_folder= 'C:\\Users\\AmyYun\\OneDrive - Kubrick Group\\10k_filings_clean'
    # This is the part for sentiment word count for each file 
    empty_dict_list = []

    for filename in os.listdir(input_folder):
        f = os.path.join(input_folder, filename)
        empty_dict =dict()
        list_negative= []
        list_positive= []
        list_uncertainty= []
        list_litigious= []
        list_strong_modal= []
        list_weak_modal= []
        list_constraining= []
        with open(f, 'r',encoding='utf-8') as file:
            for line in file:
                for word in line.split():
                    if word.upper() in rd.get_sentiment_word_dict()['Negative']:
                        list_negative.append(word)
                        empty_dict['Negative'] = len(list_negative)
                    if word.upper() in rd.get_sentiment_word_dict()['Positive']:
                        list_positive.append(word)
                        empty_dict['Positive'] = len(list_positive)
                    if word.upper() in rd.get_sentiment_word_dict()['Uncertainty']:
                        list_uncertainty.append(word)
                        empty_dict['Uncertainty'] = len(list_uncertainty)
                    if word.upper() in rd.get_sentiment_word_dict()['Litigious']:
                        list_litigious.append(word)
                        empty_dict['Litigious'] = len(list_litigious)
                    if word.upper() in rd.get_sentiment_word_dict()['Strong_Modal']:
                        list_strong_modal.append(word)
                        empty_dict['Strong_Modal'] = len(list_strong_modal)
                    if word.upper() in rd.get_sentiment_word_dict()['Weak_Modal']:
                        list_weak_modal.append(word)
                        empty_dict['Weak_Modal'] = len(list_weak_modal)
                    if word.upper() in rd.get_sentiment_word_dict()['Constraining']:
                        list_constraining.append(word)
                        empty_dict['Constraining'] = len(list_constraining)

        # formatting into the output format
        s = filename
        symbol = s.split("_")[0]
        report_type = s.split("_")[1]
        date_1 = s.split("_")[2].split(".")[0]
        dt_object = datetime.strptime(date_1, "%Y-%m-%d")
        format_date = dt_object.strftime('%d-%m-%y')
        empty_dict['Symbol'] =symbol
        empty_dict['ReportType'] =report_type
        empty_dict['FilingDate'] =format_date
        empty_dict_list.append(empty_dict)

    #This generates the dataframe 

    df = pd.DataFrame(empty_dict_list, columns=['Symbol', 'ReportType', 'FilingDate', 
    'Negative', 'Positive', 'Uncertainty', 'Litigious', 'Strong_Modal', 'Weak_Modal', 'Constraining'])
    df.to_csv(output_file, index=False)