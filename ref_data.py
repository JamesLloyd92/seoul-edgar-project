import pandas as pd
from yahoofinancials import YahooFinancials

# The function below reads the current list of S&P100 from wikipedia, then changes
# the format so the yahoo financials module can read it.
def get_sp100():
    snp100 = pd.read_html('https://en.wikipedia.org/wiki/S%26P_100')[2]['Symbol'].tolist()
    result = [w.replace('.', '-') for w in snp100]
    return result


# This function gets the yahoo financial data from the start and end date
# specified, and from a given list of stock ticker. 
def get_yahoo_data(start_date = str, end_date = str, tickers = list):
#   Here this first part takes the list of tickers and gets the stock data for
#   them and stores it in a dictionary, with the key being the ticker
#   and the value being the json data.
    all_data = {}
    for ticker in tickers:
        all_data[ticker] = YahooFinancials(ticker).get_historical_price_data(start_date, end_date, 'daily')
#   This below creates a dataframe from a given ticker with the relevant extra columns and
#   adds it to a list - the list contains a dataframe for each ticker.
    dataframes = []
    for ticker in all_data:
        prices = pd.DataFrame(all_data[ticker][ticker]['prices'])
        prices['1daily_return'] = prices['close'].pct_change()
        prices['2daily_return'] = prices['close'].pct_change(periods=2)
        prices['3daily_return'] = prices['close'].pct_change(periods=3)
        prices['5daily_return'] = prices['close'].pct_change(periods=5)
        prices['10daily_return'] = prices['close'].pct_change(periods=10)
        prices['symbol'] = ticker
        prices.drop(columns = 'date', inplace=True)
        prices.drop(columns = 'adjclose', inplace=True)
        dataframes.append(prices)
#   Here the list of dataframes is iterated over and the final dataframe is
#   concatenated from all the elements. 
    df1 = pd.DataFrame(dataframes[0])
    for frame in range(len((dataframes))-1):
        df1 = pd.concat([df1, dataframes[frame+1]], axis = 0)

    return df1


# For example, the following code downloads some data from Yahoo financials,
# and saves it to a csv with the specified path.

# import ref_data as rd
# from pathlib import Path  
# results = rd.get_yahoo_data('2021-07-01','2022-01-01', rd.get_sp100())
# filepath = Path('../seoul_edgar_project/stock_returns_daily.csv')  
# filepath.parent.mkdir(parents=True, exist_ok=True)  
# results.to_csv(filepath) 

import pandas as pd
from pandas import *
# THis is repeating 7 times to produce a dictionary for each sentiment
d = pd.read_excel('..\edgar_package\\Loughran-McDonald_MasterDictionary_1993-2021.xlsx', engine='openpyxl')
d = d.set_index(['Word'])["Negative"].to_dict()
empty_list = []
for words, negatives in d.items():
    if negatives !=0:
        empty_list.append(words)
dict_of_negatives = {}
dict_of_negatives['Negative'] = empty_list

d = pd.read_excel('..\edgar_package\\Loughran-McDonald_MasterDictionary_1993-2021.xlsx', engine='openpyxl')
d = d.set_index(['Word'])["Positive"].to_dict()
list_of_words_of_positive = []
for words, positives in d.items():
    if positives !=0:
        list_of_words_of_positive.append(words)
dict_of_positives = {}
dict_of_positives['Positive'] = list_of_words_of_positive

d = pd.read_excel('..\edgar_package\\Loughran-McDonald_MasterDictionary_1993-2021.xlsx', engine='openpyxl')
d = d.set_index(['Word'])["Uncertainty"].to_dict()
list_of_words_of_uncertainty = []
for words, uncertainty in d.items():
    if uncertainty !=0:
        list_of_words_of_uncertainty.append(words)
dict_of_uncertainty = {}
dict_of_uncertainty ['Uncertainty'] = list_of_words_of_uncertainty 

d = pd.read_excel('..\edgar_package\\Loughran-McDonald_MasterDictionary_1993-2021.xlsx', engine='openpyxl')
d = d.set_index(['Word'])["Litigious"].to_dict()
list_of_words_of_litigious = []
for words, litigious in d.items():
    if litigious !=0:
        list_of_words_of_litigious.append(words)
dict_of_litigious = {}
dict_of_litigious ['Litigious'] = list_of_words_of_litigious

d = pd.read_excel('..\edgar_package\\Loughran-McDonald_MasterDictionary_1993-2021.xlsx', engine='openpyxl')
d = d.set_index(['Word'])["Strong_Modal"].to_dict()
list_of_words_of_strong_modal = []
for words, strong_modal in d.items():
    if strong_modal!=0:
        list_of_words_of_strong_modal.append(words)
dict_of_strong_modal = {}
dict_of_strong_modal ['Strong_Modal'] = list_of_words_of_strong_modal

d = pd.read_excel('..\edgar_package\\Loughran-McDonald_MasterDictionary_1993-2021.xlsx', engine='openpyxl')
d = d.set_index(['Word'])["Weak_Modal"].to_dict()
list_of_words_of_weak_modal = []
for words, weak_modal in d.items():
    if weak_modal!=0:
        list_of_words_of_weak_modal.append(words)
dict_of_weak_modal = {}
dict_of_weak_modal ['Weak_Modal'] = list_of_words_of_weak_modal

d = pd.read_excel('..\edgar_package\\Loughran-McDonald_MasterDictionary_1993-2021.xlsx', engine='openpyxl')
d = d.set_index(['Word'])["Constraining"].to_dict()
list_of_words_of_constraining = []
for words, constraining in d.items():
    if constraining!=0:
        list_of_words_of_constraining.append(words)
dict_of_constraining = {}
dict_of_constraining ['Constraining'] = list_of_words_of_constraining

# THis merges all the dictionary produced above and makes one whole dictionary
whole_dict_lm = {}
whole_dict_lm = dict_of_negatives | dict_of_positives| dict_of_uncertainty| dict_of_litigious | dict_of_strong_modal | dict_of_weak_modal | dict_of_constraining


# You only need to run the code above once, then calling the sentiment function
# returns the result instantly, not every 1.5 mins.
def get_sentiment_word_dict():
    return whole_dict_lm

