from selenium import webdriver
import time
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
def download_files_10k(ticker = str, dest_folder = str):
    
    driver = webdriver.Chrome('./chromedriver') 
  
    driver.get(f'https://www.sec.gov/edgar/search/#/dateRange=all&category=custom&forms=10-K')
    time.sleep(3) 
    # ----------------------------------------------------------------------- NEW CODE
    inputs = driver.find_element(By.XPATH, r'//*[@id="entity-full-form"]')
    inputs.click()
    time.sleep(1) 
    inputs.send_keys(ticker)
    time.sleep(1)
    inputs.send_keys(Keys.ARROW_DOWN)
    time.sleep(1)
    inputs.send_keys(Keys.ENTER)
    time.sleep(1)
    # ----------------------------------------------------------------------- NEW CODE
    html_source = driver.page_source
    soup = BeautifulSoup(html_source,'html.parser')
    tables = soup.find_all('table')
    table_rows = tables[4].find_all('tr')
    for row in table_rows[1:]:
        time.sleep(1)
        columns = row.find_all('td')
        link = columns[0].a
        date = columns[1].get_text()
        second = link['data-adsh'].replace('-','')
        third = link['data-file-name']
        first = columns[4].get_text().split()[1].lstrip('0')
        url = f'https://www.sec.gov/Archives/edgar/data/{first}/{second}/{third}'
        driver.get(url)
        page_data = driver.page_source 

        with open(f'{dest_folder}/{ticker}_10-k_{date}.htm','w', encoding = 'utf-8') as save_file:
            save_file.write(page_data)

    driver.close()

# example use of function:
# import edgar_downloader as ed
# ed.download_files_10k('AAPL', r'C:\Users\JamesLloyd\Python\edgar_assessment\edgar_package\10k_filings_raw')